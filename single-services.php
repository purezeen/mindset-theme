<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gulp-wordpress
 */
get_header(); ?>

<main class="container">
   <div class="row">
      <div class="post-hero col-12">
         <img src="<?php echo get_template_directory_uri()?>/img/services-hero.png" alt="">
      </div>

      <div class="services-post_header col-12">
         <h1>RETREAT - TIMSKI ODMOR – TEAM BUILDING</h1>

         <div>
            <p>Trajanje: <strong>90-120 min</strong></p>
            <p>Grupa: <strong>preko 25</strong></p>
         </div>
      </div>

      <div class="col-12 editor">
         <?php		
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', get_post_format() );

			endwhile; // End of the loop.
			?>

         <div class="border-box_wrap">
            <div class="border-box">
               <h6><strong>Modul 3</strong> <br>Prezentacijske i govorničke veštine</h6>
               <p>Cilj modula je da se učesnici osposobe za izlaganje i komunikaciju sa slušaocima koji su adekvatne za
                  cilj i sadržaj javnog nastupa, uz korištenje adekvatnih audio i vizuelnih pomagala. Biće razvijene
                  veštine jasnog izlaganja, adekvatnog slušanja, postavljanja pitanja učesnicima javnog nastupa.
                  Metode rada na modulu su interaktivna diskusija, demonstracije, akvarijum tehnika, vežbe u malim
                  grupama. </p>
            </div>
            <div class="border-box">
               <h6><strong>Modul 3</strong> <br>Prezentacijske i govorničke veštine</h6>
               <p>Cilj modula je da se učesnici osposobe za izlaganje i komunikaciju sa slušaocima koji su adekvatne za
                  cilj i sadržaj javnog nastupa, uz korištenje adekvatnih audio i vizuelnih pomagala. Biće razvijene
                  veštine jasnog izlaganja, adekvatnog slušanja, postavljanja pitanja učesnicima javnog nastupa.
                  , akvarijum tehnika, vežbe u malim grupama. </p>
            </div>
            <div class="border-box">
               <h6><strong>Modul 3</strong> <br>Prezentacijske i govorničke veštine</h6>
               <p>Cilj modula je da se učesnici osposobe za izlaganje i komunikaciju sa slušaocima koji su adekvatne za
                  cilj i sadržaj javnog nastupa, uz korištenje adekvatnih audio i vizuelnih pomagala. Biće razvijene
                  veštine jasnog izlaganja, adekvatnog slušanja, postavljanja pitanja učesnicima javnog nastupa.
                  Metode rada na modulu su interaktivna diskusija, demonstracije, akvarijum tehnika, vežbe u malim
                  grupama. </p>
            </div>
            <div class="border-box">
               <h6><strong>Modul 3</strong> <br>Prezentacijske i govorničke veštine</h6>
               <p>Cilj modula je da se učesnici osposobe za izlaganje i komunikaciju sa slušaocima koji su adekvatne za
                  cilj i sadržaj javnog nastupa, uz korištenje adekvatnih audio i vizuelnih pomagala. Biće razvijene
                  veštine jasnog izlaganja, adekvatnog slušanja, postavljanja pitanja učesnicima javnog nastupa.
            </div>
         </div>
      </div>
   </div>
</main>

<section class="section background-grey">
   <div class="section-form center form-two-columns clearfix">
      <div class="section-form_header">
         <h5>ZANIMA VAS VIŠE O TEAM BUILDINGU</h5>
         <p class="subtitle">PIŠITE NAM</p>
      </div>
      <?php echo do_shortcode('[contact-form-7 id="57" title="Untitled"]') ?>
   </div>
</section>

<section class="services-slider section">

   <div class="row">
      <div class="col-12 d-flex justify-content-center">
         <h2 class="section-title mb-big"><span class="mark"></span>Usluge</h2>
      </div>
   </div>

   <div class="center">
      <div class="container">
         <a class="row justify-content-center">
            <div class="col-12 col-sm-6">
               <div class="services_content">
                  <h3>KLASICNE OBUKE</h3>
                  <p class="subtitle">Trajanje: <strong>90-120 min</strong> Broj: <strong>preko
                        25</strong></p>
                  <p>Nasa predavanja mogu da se prilagode vasim potrebama, mogu se odrzati kod vas ili u
                     nekom drugom
                     okruzenju koje vi izaberete za svoje zaposlene. Mogu biti u toku ili nakon radnog
                     vremena, a mozda i
                     vikendom...</p>
                  <span class="btn-link btn-link_green">Saznaj više</span>
               </div>
            </div>
            <div class="col-0 col-sm-6 services_image">
               <div class="services_shapes">
                  <div class="services_circle"
                     style="background-image: linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%), url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
                  </div>
                  <div class="circle-bigger"></div>
                  <div class="circle-small"></div>
               </div>
            </div>
         </a>
      </div>
      <div class="container">
         <a class="row justify-content-center">
            <div class="col-12 col-sm-6">
               <div class="services_content">
                  <h3>KLASICNE OBUKE</h3>
                  <p class="subtitle">Trajanje: <strong>90-120 min</strong> Broj: <strong>preko
                        25</strong></p>
                  <p>Nasa predavanja mogu da se prilagode vasim potrebama, mogu se odrzati kod vas ili u
                     nekom drugom
                     okruzenju koje vi izaberete za svoje zaposlene. Mogu biti u toku ili nakon radnog
                     vremena, a mozda i
                     vikendom...</p>
                  <span class="btn-link btn-link_green">Saznaj više</span>
               </div>
            </div>
            <div class="col-0 col-sm-6 services_image">
               <div class="services_shapes">
                  <div class="services_circle"
                     style="background-image: linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%), url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
                  </div>
                  <div class="circle-bigger"></div>
                  <div class="circle-small"></div>
               </div>
            </div>
         </a>
      </div>
      <div class="container">
         <a class="row justify-content-center">
            <div class="col-12 col-sm-6">
               <div class="services_content">
                  <h3>KLASICNE OBUKE</h3>
                  <p class="subtitle">Trajanje: <strong>90-120 min</strong> Broj: <strong>preko
                        25</strong></p>
                  <p>Nasa predavanja mogu da se prilagode vasim potrebama, mogu se odrzati kod vas ili u
                     nekom drugom
                     okruzenju koje vi izaberete za svoje zaposlene. Mogu biti u toku ili nakon radnog
                     vremena, a mozda i
                     vikendom...</p>
                  <span class="btn-link btn-link_green">Saznaj više</span>
               </div>
            </div>
            <div class="col-0 col-sm-6 services_image">
               <div class="services_shapes">
                  <div class="services_circle"
                     style="background-image: linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%), url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
                  </div>
                  <div class="circle-bigger"></div>
                  <div class="circle-small"></div>
               </div>
            </div>
         </a>
      </div>
   </div>

   <div class="paginator center text-color text-center">
      <ul class="unstyle-list">
         <li class="prev"></li>
         <span class="pagingInfo"></span>
         <li class="next"></li>
      </ul>
   </div>
</section>

<section class="container border-top section">
   <div class="row">
      <div class="col-12 text-center">
         <h1 class="section-title mb-big"><span class="mark"></span>KALENDAR DOGAĐAJA</h1>
      </div>
   </div>

   <div class="event-list relative">

      <a class="event" href="#events/auto-draft/">
         <div class="event-header">
            <div class="event_data">
               <div class="big">
                  <p> 16</p> <span><br>JUL</span>
               </div>
               <div class="small">
                  <em>-</em> 26<span><br>JUL</span>
               </div>
            </div>
            <div class="event_type">predavanje</div>
         </div>
         <div class="event_content">
            <h4>SLIDEDOWN EVENT</h4>
            <p class="icon-time-grey">(July 16) 6:00 Am - (September 26) 9:00 Am</p>
            <p class="icon-location-grey">Irving City Park, NE Fremont St, Portland, OR 97212</p>
         </div>
      </a>

      <a class="event event_mark" href="#events/auto-draft/">
         <div class="event-header">
            <div class="event_data">
               <div class="big">
                  <p> 16</p><span><br>JUL</span>
               </div>
               <div class="small"><em>-</em> 26<span><br>JUL</span></div>
            </div>
         </div>
         <div class="event_content">
            <h4>EVENT WITH FEATURED IMAGE</h4>
            <p>4:00 Pm - 10:00 Pm</p>
            <p>Portland Bridges, 400 Southwest Kingston Avenue Portland, OR</p>
         </div>
      </a>

      <a class="event" href="#events/auto-draft/">
         <div class="event-header">
            <div class="event_data">
               <div class="big">
                  <p> 16</p> <span><br>JUL</span>
               </div>
               <div class="small">
                  <em>-</em> 26<span><br>JUL</span>
               </div>
            </div>
            <div class="event_type">predavanje</div>
         </div>
         <div class="event_content">
            <h4>MULTI DAY EVENT</h4>
            <p>(July 16) 6:00 Am - (September 26) 9:00 Am</p>
            <p>Irving City Park, NE Fremont St, Portland, OR 972122</p>
         </div>
      </a>

   </div>

   <div class="row justify-content-center">
      <div class="col-12 text-center">
         <a href="" class="btn-full btn-light-green btn-center mt-5">Ostali događaji</a>
      </div>
   </div>


</section>

<section class="background-green cta-box">
   <div class="container">
      <div class="row justify-content-end">
         <div class="col-12 col-sm-3 img">
            <img src="<?php echo get_template_directory_uri()?>/img/group_letters.svg">
         </div>
         <div class="col-12 col-sm-6">
            <h3>IMATE PITANJE ZA NAS?</h3>
            <p>Slobodno nas kontaktiraje</p>
            <a href="#" class="btn-full btn-dark btn-arrow">Pišite nam</a>
         </div>
      </div>
   </div>
</section>

<?php
get_footer();