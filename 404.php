<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package gulp-wordpress
 */



get_header(); ?>

<div class="site-content container section">
	<main id="main" class="site-main row" role="main">
		<div class="post-header col-12">
			<h1>NOTHING FOUND</h1>
			<hr>
		</div>
	</main><!-- #main -->
</div><!-- #primary -->

<section class="latest-post container section">
	<a>
		<div class="latest-post_image cover"
			style="background-image:url(<?php echo get_template_directory_uri()?>/img/blog1.png)">
		</div>
		<div class="post-content">
			<h5>Neverbalna komunikacija</h5>
			<p class="subtitle">07.04.1019. &nbsp; | &nbsp; <span>Kurs</span></p>
			<p>Iako mi se čini izuzetno dualistički,  tamu, ovi opaženi kvaliteti
				postoje kao takvi, ako ne kroz perspektivu apsoluta, onda kao kreacija ljudskog uma, sa ciljem
				razumevanja i raslojavanja iskustva, vrlo...</p>

			<span href="" class="btn-link btn-link_green">Saznaj više</span>
		</div>
	</a>
	<a>
		<div class="latest-post_image cover"
			style="background-image:url(<?php echo get_template_directory_uri()?>/img/blog2.png)">
		</div>
		<div class="post-content">
			<h5>Neverbalna komunikacija</h5>
			<p class="subtitle">07.04.1019. &nbsp; | &nbsp; <span>Kurs</span></p>
			<p>Iako mi se čini izuzetno dualistički, da stvarnost segmentiramo na svetlost i tamu, ovi opaženi kvaliteti
				postoje kao takvi, ako ne kroz perspektivu apsoluta, onda kao kreacija ljudskog uma, sa ciljem
				razumevanja i raslojavanja iskustva, vrlo...</p>

			<span href="" class="btn-link btn-link_green">Saznaj više</span>
		</div>
	</a>
	<a>
		<div class="latest-post_image cover"
			style="background-image:url(<?php echo get_template_directory_uri()?>/img/blog3.png)">
		</div>
		<div class="post-content">
			<h5>Neverbalna komunikacija</h5>
			<p class="subtitle">07.04.1019. &nbsp; | &nbsp; <span>Kurs</span></p>
			<p>Iako mi se čini izuzetno dualistički, da stvarnost segmentiramo na svetlost i tamu, ovi opaženi kvaliteti
				razumevanja i raslojavanja iskustva, vrlo...</p>

			<span href="" class="btn-link btn-link_green">Saznaj više</span>
		</div>
	</a>
</section>

<?php
get_footer();
