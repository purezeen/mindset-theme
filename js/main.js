/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */
(function ($) {


  $(document).ready(function () {


    /*----------  Scroll to top  ----------*/

    $(window).scroll(function () {
      if ($(this).scrollTop() >= 200) { 
        $('.scroll').css('display', 'flex') 
      } else {
        $('.scroll').fadeOut(); 
      }
    });

    $('.scroll').click(function () {
      $('html, body').animate({
        scrollTop: 0
      }, 800);
      return false;
    });


    /*----------  Smoot scroll  ----------*/



    $('a[href*=\\#]:not([href=\\#])').click(function () {
      if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
          $('html,body').animate({
            scrollTop: target.offset().top - 100
          }, 1000);
          return false;
        }
      }
    });


    /*----------  Slick slider  ----------*/

    $(window).load(function () {


      var $status = $('.pagingInfo');
      var $slickElement = $('.services-slider .center');


      $slickElement.on('load init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (currentSlide ? currentSlide : 0) + 1;
        $status.text('0' + i + ' / 0' + slick.slideCount);
      });


      $('.services-slider .center').slick({
        centerMode: true,
        centerPadding: '30px',
        slidesToShow: 1,
        mobileFirst: true,
        arrows: true,
        prevArrow: $('.services-slider .prev'),
        nextArrow: $('.services-slider .next'),
        responsive: [{
            breakpoint: 1600,
            settings: {
              arrows: false,
              centerMode: true,
              centerPadding: '20%',
              slidesToShow: 1
            }
          },
          {
            breakpoint: 1450,
            settings: {
              arrows: false,
              centerMode: true,
              centerPadding: '250px',
              slidesToShow: 1
            }
          },
          {
            breakpoint: 990,
            settings: {
              arrows: false,
              centerMode: true,
              centerPadding: '150px',
              slidesToShow: 1
            }
          },
          {
            breakpoint: 760,
            settings: {
              arrows: false,
              centerMode: true,
              centerPadding: '70px',
              slidesToShow: 1
            }
          },
          {
            breakpoint: 320,
            settings: {
              arrows: false,
              centerMode: true,
              centerPadding: '40px',
              slidesToShow: 1
            }
          },

        ]
      });

      $('.fade').slick({
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        prevArrow: $('.testimonial .prev'),
        nextArrow: $('.testimonial .next')
      });

    });
  });


  /*----------  Button : Show more, show less  ----------*/


  $(document).ready(function () {
    // Configure/customize these variables.
    var showChar = 500; // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "PROČITAJ VIŠE";
    var lesstext = "PROČITAJ MANJE";


    $('.more').each(function () {
      var content = $(this).html();


      if (content.length > showChar) {

        var c = content.substr(0, showChar);
        var h = content.substr(showChar, content.length - showChar);

        var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink btn-link btn-link_light_green">' + moretext + '</a></span>';

        $(this).html(html);
      }
    });

    $(".morelink").click(function () {
      if ($(this).hasClass("less")) {
        $(this).removeClass("less");
        $(this).html(moretext);
      } else {
        $(this).addClass("less");
        $(this).html(lesstext);
      }
      $(this).parent().prev().toggle();
      $(this).prev().toggle();
      return false;
    });


    /*----------  Modal box  ----------*/


    $('.open-modal').on('click', function () {
      $('.modal').addClass('modal-show');
    });

    $('.contact_close').on('click', function () {
      $('.modal').removeClass('modal-show');
    });


    $(".open-modal").click(function (event) {
      event.preventDefault();
    })

    $(window).on('load resize', function () {
      if ($(window).width() >= 992) {
        var pTags = $(".navbar");

        if (pTags.parent().is(".mobile-wrap")) {
          pTags.unwrap();
        }
      }
    });

    $('.burger').on('click', function () {
      $('.mobile-wrap').addClass('height');
    });

    $('.nav-close').on('click', function () {
      $('.mobile-wrap').removeClass('height');
    });
  });

  
  $(".wpcf7-list-item.first label").addClass('checked');
     
  $("input[type=radio]").click(function () {
    if ($(this).not("checked")) {
      $('.section-form .radio label').removeClass('checked');
      $(this).parent().addClass('checked');
    }
  });



})(jQuery);