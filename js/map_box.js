(function ($) {

  $(document).ready(function () {

    // if($('body').is('.home, .page-template-contact')) {

    // This will let you use the .remove() function later on
    if (!('remove' in Element.prototype)) {
      Element.prototype.remove = function () {
        if (this.parentNode) {
          this.parentNode.removeChild(this);
        }
      };
    }

    mapboxgl.accessToken = 'pk.eyJ1IjoiaXZhbmFpa2EiLCJhIjoiY2p3MG1nY2lkMGNrMDRhcndzNDVyZXoyaSJ9.STwhzlwrUlcvPQUDkFr1Zg';

    // This adds the map
    var map = new mapboxgl.Map({
      // container id specified in the HTML
      container: 'map',
      // style URL
      style: 'mapbox://styles/ivanaika/ck1vxjzqk0a2c1cp8oyncw21h',
      // initial position in [long, lat] format
      center: [20.510719, 44.794230],
      // initial zoom
      zoom: 15,
    });

    // disable map zoom when using scroll
    map.scrollZoom.disable();

    var popup = new mapboxgl.Popup({
        closeOnClick: false
      })
      .setLngLat([20.516629, 44.791978])
      // .setHTML('<div class="map_popup"><p>Ulica Matice srpske, Beograd<br>telefon: <strong> 063/753 2230 <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;063/753 2230 </strong></p></div>')
      .addTo(map);

    var size = 200;

    var pulsingDot = {
      width: size,
      height: size,
      data: new Uint8Array(size * size * 4),

      onAdd: function () {
        var canvas = document.createElement('canvas');
        canvas.width = this.width;
        canvas.height = this.height;
        this.context = canvas.getContext('2d');
      },

      render: function () {
        var duration = 1000;
        var t = (performance.now() % duration) / duration;

        var radius = size / 2 * 0.3;
        var outerRadius = size / 5 * 0.7 * t + radius;
        var context = this.context;

        // draw outer circle
        context.clearRect(0, 0, this.width, this.height);
        context.beginPath();
        context.arc(this.width / 2, this.height / 2, outerRadius, 0, Math.PI * 2);
        context.fillStyle = 'rgba(228, 186, 46,' + (1 - t) + ')';
        context.fill();

        // draw inner circle
        context.beginPath();
        context.arc(this.width / 2, this.height / 2, radius, 0, Math.PI * 2);
        context.fillStyle = 'rgba(228, 186, 46, 1)';
        context.strokeStyle = 'rgba(228, 186, 46, .8)';
        context.lineWidth = 2 + 2 * (1 - t);
        context.fill();
        context.stroke();

        // update this image's data with data from the canvas
        this.data = context.getImageData(0, 0, this.width, this.height).data;

        // keep the map repainting
        map.triggerRepaint();

        // return `true` to let the map know that the image was updated
        return true;
      }
    };


    map.on('load', function () {

      map.addLayer({
        "id": "points",
        "type": "symbol",
        "source": {
          "type": "geojson",
          "data": {
            "type": "FeatureCollection",
            "features": [{
              "type": "Feature",
              "geometry": {
                "type": "Point",
                "coordinates": [20.510719, 44.794230]
              },
              "properties": {
                "title": "Elixir Star",
                "description": "Lokacija zgrade",
                "icon": "https://img.icons8.com/dusk/32/000000/skyscrapers.png",
                "distance": "0m"
              }
            }]
          }
        },
        "layout": {
          "icon-image": "pulsing-dot"
        }
      });
    });
  // }
  });
})(jQuery);