<?php /* Template Name: Services */ get_header(); ?>

<section class="hero cover opacity"
   style="background-image:linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%) , url(<?php echo get_template_directory_uri()?>/img/usluge-hero.png)">
</section>

<section class="section curve-white curve-padding-top">
   <div class="container services">
      <div class="section-header">
         <h2 class="section-title text-center"><span class="mark"></span>Usluge</h2>
         <p>Usluge možemo da prilagodimo isključivo vama i vašem jedinstvenom okruženju! Negujte vašu organizacionu
            kulturu, rastite i razvijajte se uz našu ponudu.</p>
      </div>

      <a href="/services/auto-draft-2/" class="row justify-content-center">
         <div class="col-12 col-sm-6">
            <div class="services_content">
               <h3>KLASICNE OBUKE</h3>
               <p class="subtitle">Trajanje: <strong>90-120 min</strong> &nbsp;	| &nbsp; Broj: <strong>preko 25</strong></p>
               <p>Nasa predavanja mogu da se prilagode vasim potrebama, mogu se odrzati kod vas ili u nekom drugom
                  okruzenju koje vi izaberete za svoje zaposlene. Mogu biti u toku ili nakon radnog vremena, a mozda i
                  vikendom...</p>
               <span class="btn-link btn-link_green">Saznaj više</span>
            </div>
         </div>
         <div class="col-0 col-sm-6 services_image">
            <div class="services_shapes">
               <div class="services_circle cover"
                  style="background-image: linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%), url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
               </div>
            </div>
         </div>
      </a>

      <a href="/services/auto-draft-2/" class="row justify-content-center">
         <div class="col-12 col-sm-6">
            <div class="services_content">
               <h3>INTERAKTIVNI TRENINZI</h3>
               <p class="subtitle">Trajanje: <strong>90-120 min</strong> &nbsp;	| &nbsp; Broj: <strong>preko 25</strong></p>
               <p>Nasa predavanja mogu da se prilagode vasim potrebama, mogu se odrzati kod vas ili u nekom drugom
                  okruzenju koje vi izaberete za svoje zaposlene. Mogu biti u toku ili nakon radnog vremena, a mozda i
                  vikendom...</p>
               <span class="btn-link btn-link_green">Saznaj više</span>
            </div>
         </div>
         <div class="col-0 col-sm-6 services_image">
         <div class="services_shapes">
               <div class="services_circle cover"
                  style="background-image: linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%), url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
               </div>
            </div>
         </div>
      </a>

      <a href="#" class="row justify-content-center">
         <div class="col-12 col-sm-6">
            <div class="services_content">
               <h3>PSIHOLOŠKA PODRŠKA, SAVETOVANJE I TERAPIJA</h3>
               <p class="subtitle">Trajanje: <strong>90-120 min</strong> &nbsp;	| &nbsp; Broj: <strong>preko 25</strong></p>
               <p>Nasa predavanja mogu da se prilagode vasim potrebama, mogu se odrzati kod vas ili u nekom drugom
                  okruzenju koje vi izaberete za svoje zaposlene. Mogu biti u toku ili nakon radnog vremena, a mozda i
                  vikendom...</p>
               <span class="btn-link btn-link_green">Saznaj više</span>
            </div>
         </div>
         <div class="col-0 col-sm-6 services_image">
         <div class="services_shapes">
               <div class="services_circle"
                  style="background-image: linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%), url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
               </div>
            </div>
         </div>
      </a>

      <a href="#" class="row justify-content-center">
         <div class="col-12 col-sm-6">
            <div class="services_content">
               <h3>KOUČING</h3>
               <p class="subtitle">Trajanje: <strong>90-120 min</strong> &nbsp;	| &nbsp; Broj: <strong>preko 25</strong></p>
               <p>Nasa predavanja mogu da se prilagode vasim potrebama, mogu se odrzati kod vas ili u nekom drugom
                  okruzenju koje vi izaberete za svoje zaposlene. Mogu biti u toku ili nakon radnog vremena, a mozda i
                  vikendom...</p>
               <span class="btn-link btn-link_green">Saznaj više</span>
            </div>
         </div>
         <div class="col-0 col-sm-6 services_image">
         <div class="services_shapes">
               <div class="services_circle cover"
                  style="background-image: linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%), url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
               </div>
            </div>
         </div>
      </a>

      <a href="#" class="row justify-content-center">
         <div class="col-12 col-sm-6">
            <div class="services_content">
               <h3>RETREAT - TIMSKI ODMOR – TEAM BUILDING </h3>
               <p class="subtitle">Trajanje: <strong>90-120 min</strong> Broj: <strong>preko 25</strong></p>
               <p>Nasa predavanja mogu da se prilagode vasim potrebama, mogu se odrzati kod vas ili u nekom drugom
                  okruzenju koje vi izaberete za svoje zaposlene. Mogu biti u toku ili nakon radnog vremena, a mozda i
                  vikendom...</p>
               <span class="btn-link btn-link_green">Saznaj više</span>
            </div>
         </div>
         <div class="col-0 col-sm-6 services_image">
         <div class="services_shapes">
               <div class="services_circle cover"
                  style="background-image: linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%), url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
               </div>
            </div>
         </div>
      </a>

      <a class="row justify-content-center">
         <div class="col-12 col-sm-6">
            <div class="services_content">
               <h3>ISTRAŽIVANJA – MERENJA ZADOVOLJSTVA POSLOM</h3>
               <p class="subtitle">Trajanje: <strong>90-120 min</strong> &nbsp;	| &nbsp; Broj: <strong>preko 25</strong></p>
               <p>Nasa predavanja mogu da se prilagode vasim potrebama, mogu se odrzati kod vas ili u nekom drugom
                  okruzenju koje vi izaberete za svoje zaposlene. Mogu biti u toku ili nakon radnog vremena, a mozda i
                  vikendom...</p>
               <span class="btn-link btn-link_green">Saznaj više</span>
            </div>
         </div>
         <div class="col-0 col-sm-6 services_image">
         <div class="services_shapes">
               <div class="services_circle cover"
                  style="background-image: linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%), url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
               </div>
            </div>
         </div>
      </a>
   </div>

   <div class="pagination center">
			<ul class="unstyle-list">
				<li class="prev"></li>
				<span class="pagination-number">01/02</span>
				<li class="next"></li>
			</ul>
		</div>
</section>



<?php get_footer(); ?>