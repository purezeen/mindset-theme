<?php /* Template Name: Pagination */ get_header(); ?>


         
         <div class="row justify-content-center pagination">
            <div class="col-12">
               <?php
                  the_posts_pagination( array(
                     'mid_size'  => 2,
                     'prev_text' => __( '<i class="arrow left"></i>', 'textdomain' ),
                     'next_text' => __( '<i class="arrow right"></i>', 'textdomain' ),
                  ) );
               ?>
            </div>
         </div>

         <style>

            .pagination {
               margin-top: 20px;
            }

            .pagination i {
               border: solid #25B5DB;
               border-width: 0 2px 2px 0;
               display: inline-block;
               padding: 3px;
            }

            .pagination .right {
               transform: rotate(-45deg);
               -webkit-transform: rotate(-45deg);
            }

            .pagination .left {
               transform: rotate(135deg);
               -webkit-transform: rotate(135deg);
            }

            .pagination .nav-links {
               margin: 0 auto;
               display: flex;
            }

            .pagination .nav-links span,
            .pagination .nav-links a {
               border: 1px solid #25B5DB;
               width: 25px;
               height: 25px;
               display: inline-block;
               text-align: center;
               display: flex;
               flex-direction: row;
               justify-content: center;
               align-items: center;
               margin: 4px;
               font-size: 14px;
               font-weight: 700;
            }

            .pagination .nav-links span:hover,
            .pagination .nav-links a:hover
            {
              
               background-color: #02617A;
               color: #fff;
               transition: all .5s;
            }

            .pagination .nav-links a:hover i {
               color: #fff;
            }

            .pagination .page-numbers.current {
               background-color: #25B5DB;
               color: #fff;
            }
            .pagination .page-numbers.current:hover  {
               background-color: #25B5DB;
            }
         
         </style>

<?php get_footer(); ?>