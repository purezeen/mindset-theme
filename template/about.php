<?php /* Template Name: About us */ get_header(); ?>
<section class="hero cover"
   style="background-image:linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%) , url(<?php echo get_template_directory_uri()?>/img/about-hero.png)">
</section>

<section class="section curve-white curve-padding-top">
   <div class="container about-us ">
      <div class="row two-columns align-items-center">
         <div class="col-12 col-md-6">
            <div class="box-content p-0">
               <div class="box-img cover"
                  style="background-image: linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%), url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
               </div>
            </div>
         </div>
         <div class="col-12 col-md-6">
            <div class="line-left box-content">
               <h2 class="section-title mb-normal"><span class="mark"></span>O NAMA</h2>
               <p>Centar za lični i profesionalni razvoj “Mindset”, osnovan je 2019. godine, kako bismo
                  primenili svoje
                  znanje i iskustvo i pružili podršku svima koji žele da rastu.</p>
               <p>Mi znamo da iako svi nemamo iste talente i sposobnosti, ipak možemo da ih razvijamo tokom
                  celog života.
                  Jednako kao što se mišići mogu ojačati vežbanjem u teretani, sposobnosti i talenti se mogu
                  razvijati i
                  unaprediti kroz adekvatnu ekspertsku podršku i prilagođene razvojne zadatke.</p>
            </div>
         </div>
      </div>
      <div class="row two-columns">
         <div class="col-12 col-md-6">
            <div class="line-left box-content">
               <h2 class="section-title mb-normal"><span class="mark"></span>USLUGE</h2>
               <p>Centar za lični i profesionalni razvoj "Mindset" nudi usluge koje su namenjene pojedincima,
                  grupama,
                  timovima, organizacijama i kompanijama koje su spremne da na kreativan i inovativan način
                  rastu. </p>
               <p>Usluge možemo da prilagodimo isključivo vama i vašem jedinstvenom okruženju! Negujte vašu
                  organizacionu
                  kulturu, rastite i razvijajte se uz našu ponudu.</p>
               <a href="/usluge/?lang=sr" class="btn-link btn-link_light_green">pogledaj sve usluge</a>
            </div>
         </div>
         <div class="col-12 col-md-6">
            <div class="box-content p-0">
               <div class="box-img cover"
                  style="background-image: linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%), url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
               </div>
            </div>
         </div>
      </div>
   </div>

</section>

<section class="services-slider section curve-padding-bottom">
   <div class="center">
      <div class="container">
         <a class="row justify-content-center">
            <div class="col-12 col-sm-6">
               <div class="services_content">
                  <h3>KLASICNE OBUKE</h3>
                  <p class="subtitle">Trajanje: <strong>90-120 min</strong> Broj: <strong>preko
                        25</strong></p>
                  <p>Nasa predavanja mogu da se prilagode vasim potrebama, mogu se odrzati kod vas ili u
                     nekom drugom
                     okruzenju koje vi izaberete za svoje zaposlene. Mogu biti u toku ili nakon radnog
                     vremena, a mozda i
                     vikendom...</p>
                  <span class="btn-link btn-link_green">Saznaj više</span>
               </div>
            </div>
            <div class="col-0 col-sm-6 services_image">
               <div class="services_shapes">
                  <div class="services_circle"
                     style="background-image: linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%), url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
                  </div>
                  <div class="circle-bigger"></div>
                  <div class="circle-small"></div>
               </div>
            </div>
         </a>
      </div>
      <div class="container">
         <a class="row justify-content-center">
            <div class="col-12 col-sm-6">
               <div class="services_content">
                  <h3>KLASICNE OBUKE</h3>
                  <p class="subtitle">Trajanje: <strong>90-120 min</strong> Broj: <strong>preko
                        25</strong></p>
                  <p>Nasa predavanja mogu da se prilagode vasim potrebama, mogu se odrzati kod vas ili u
                     nekom drugom
                     okruzenju koje vi izaberete za svoje zaposlene. Mogu biti u toku ili nakon radnog
                     vremena, a mozda i
                     vikendom...</p>
                  <span class="btn-link btn-link_green">Saznaj više</span>
               </div>
            </div>
            <div class="col-0 col-sm-6 services_image">
               <div class="services_shapes">
                  <div class="services_circle"
                     style="background-image: linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%), url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
                  </div>
                  <div class="circle-bigger"></div>
                  <div class="circle-small"></div>
               </div>
            </div>
         </a>
      </div>
      <div class="container">
         <a class="row justify-content-center">
            <div class="col-12 col-sm-6">
               <div class="services_content">
                  <h3>KLASICNE OBUKE</h3>
                  <p class="subtitle">Trajanje: <strong>90-120 min</strong> Broj: <strong>preko
                        25</strong></p>
                  <p>Nasa predavanja mogu da se prilagode vasim potrebama, mogu se odrzati kod vas ili u
                     nekom drugom
                     okruzenju koje vi izaberete za svoje zaposlene. Mogu biti u toku ili nakon radnog
                     vremena, a mozda i
                     vikendom...</p>
                  <span class="btn-link btn-link_green">Saznaj više</span>
               </div>
            </div>
            <div class="col-0 col-sm-6 services_image">
               <div class="services_shapes">
                  <div class="services_circle"
                     style="background-image: linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%), url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
                  </div>
                  <div class="circle-bigger"></div>
                  <div class="circle-small"></div>
               </div>
            </div>
         </a>
      </div>
   </div>

   <div class="paginator center text-color text-center">
      <ul class="unstyle-list">
         <li class="prev"></li>
         <span class="pagingInfo"></span>
         <li class="next"></li>
      </ul>
   </div>
</section>

<section class="section team-home curve-grey curve-padding-top background-grey">
   <div class="container">
      <div class="row justify-content-center">
         <div class="col-12 col-lg-7 text-center team-home_title_box">
            <h2 class="section-title mb-normal center"><span class="mark"></span>NAŠ TIM</h2>
            <p class="mb-big">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
               Ipsum has been the industry's </p>
         </div>
      </div>

      <div class="team-home_wrap">

         <a href="/tim/?lang=sr" class="team-home_box">
            <div class="team-home_img">
               <img src="<?php echo get_template_directory_uri()?>/img/team1.png" alt="Team">
            </div>
            <div class="team-home_content">
               <hr>
               <h6>Ivana Koprivica</h6>
               <p>CEO of Pure Engine</p>
            </div>
         </a>
         <a href="/tim/?lang=sr" class="team-home_box">
            <div class="team-home_img">
               <img src="<?php echo get_template_directory_uri()?>/img/team2.png" alt="Team">
            </div>
            <div class="team-home_content">
               <hr>
               <h6>Marina Vukotić</h6>
               <p>CEO of Pure Engine</p>
            </div>
         </a>
         <a href="/tim/?lang=sr" class="team-home_box">
            <div class="team-home_img">
               <img src="<?php echo get_template_directory_uri()?>/img/team3.png" alt="Team">
            </div>
            <div class="team-home_content">
               <hr>
               <h6>Tatjana Lazor Obradović</h6>
               <p>CEO of Pure Engine</p>
            </div>
         </a>
         <a href="/tim/?lang=sr" class="team-home_box">
            <div class="team-home_img">
               <img src="<?php echo get_template_directory_uri()?>/img/team4.png" alt="Team">
            </div>
            <div class="team-home_content">
               <hr>
               <h6>Mirjana Beara</h6>
               <p>CEO of Pure Engine</p>
            </div>
         </a>

      </div>

      <div class="d-flex">
         <a href="/tim/?lang=sr" class="btn-full btn-light-green btn-center relative">Upoznajte tim</a>
      </div>
   </div>
</section>

<section class="background-green cta-box ">
   <div class="container">
      <div class="row justify-content-end">
         <div class="col-12 col-sm-3 img">
            <img src="<?php echo get_template_directory_uri()?>/img/group_letters.svg">
         </div>
         <div class="col-12 col-sm-6">
            <h3>IMATE PITANJE ZA NAS?</h3>
            <p>Slobodno nas kontaktiraje</p>
            <a href="#" class="btn-full btn-dark btn-arrow">Pišite nam</a>
         </div>
      </div>
   </div>
</section>

<?php get_footer(); ?>