<?php /* Template Name: Team */ get_header(); ?>
<section class="hero cover"
    style="background-image:linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%) , url(<?php echo get_template_directory_uri()?>/img/about-hero.png)">
</section>


<section class="team curve-white section curve-padding-top">
    <div class="container">

        <div class="row">
            <div class="col-12 col-sm-4">
                <div class="team_image">
                    <img src="<?php echo get_template_directory_uri()?>/img/team1.png" alt="Team">
                </div>
            </div>
            <div class="col-12 col-sm-8 team_content">
                <h2 class="section-title mb-normal"><span class="mark"></span>IVANA KOPRIVICA</h2>

                <p class="subtitle">CEO OF PURE ENGINE</p>
                <p class="more">
                    Završila osnovne studije psihologije na Filozofskom fakultetu Univerziteta u Novom Sadu, kao i
                    specijalističke jednogodišnje studije iz oblasti poslovne psihologije. Master studije završila je na
                    Fakultetu za pravne i poslovne studije „Dr. Lazar Vrkatić“ na studijskom programu poslovna
                    psihologija. U poslednje 4 godine radi na poziciji direktorke Centra za porodični smeštaj i
                    usvojenje Novi Sad.
                    Prethodno radno iskustvo je dominantno u oblasti socijalne zaštite imala kroz rad na poslovima
                    savetnice
                    <br> <br>

                    U poslednje 4 godine radi na poziciji direktorke Centra za porodični smeštaj i usvojenje Novi Sad.
                    Prethodno radno iskustvo je dominantno u oblasti socijalne zaštite imala kroz rad na poslovima
                    savetnice
                    u Pokrajinskom zavodu za socijalnu zaštitu, kao i kroz rad u civilnom sektoru.<br> <br>

                    Pripremala je i realizovala brojne domaće i međunarodne projekte. Koautorka i realizatorka je
                    nekoliko
                    akreditovanih programa za stručno usavršavanje zaposlenih u sistemu socijalne zaštite. Autorka je
                    nekoliko priručnika i stručnih publikacija. Realizovala je obuke i za zaposlene u IT sektoru. <br>
                    <br>

                    Aktivna trenerica za različite edukativne/trening programe u poslednjih 17 godina. Sertifikovani je
                    kouč
                    za organizacioni kognitivno-bihejvioralni koučing. Aktuelno na završnom nivou edukacije za
                    psihoterapeuta REBT orijentacije u okviru pridruženog trening centar Instituta Albert Elis iz
                    Njujorka.
                    <br> <br>

                    Majka dvoje dece, aktivno trenira pilates preko 10 godina, spremna na timski rad u poslu i životu,
                    voli
                    putovanja, muziku i dobre žurke.<br>

                </p>
                <p></p>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-sm-4">
                <div class="team_image">
                    <img src="<?php echo get_template_directory_uri()?>/img/team2.png" alt="Team">
                </div>
            </div>
            <div class="col-12 col-sm-8 team_content">
                <h2 class="section-title mb-normal"><span class="mark"></span>IVANA KOPRIVICA</h2>
                <p class="subtitle">CEO OF PURE ENGINE</p>
                <p class="more">

                    Završila osnovne studije psihologije na Filozofskom fakultetu Univerziteta u Novom Sadu, kao i
                    specijalističke jednogodišnje studije iz oblasti poslovne psihologije. Master studije završila je na
                    Fakultetu za pravne i poslovne studije „Dr. Lazar Vrkatić“ na studijskom programu poslovna
                    psihologija.
                    <br> <br>

                    U poslednje 4 godine radi na poziciji direktorke Centra za porodični smeštaj i usvojenje Novi Sad.
                    Prethodno radno iskustvo je dominantno u oblasti socijalne zaštite imala kroz rad na poslovima
                    savetnice
                    u Pokrajinskom zavodu za socijalnu zaštitu, kao i kroz rad u civilnom sektoru.<br> <br>

                    Pripremala je i realizovala brojne domaće i međunarodne projekte. Koautorka i realizatorka je
                    nekoliko
                    akreditovanih programa za stručno usavršavanje zaposlenih u sistemu socijalne zaštite. Autorka je
                    nekoliko priručnika i stručnih publikacija. Realizovala je obuke i za zaposlene u IT sektoru. <br>
                    <br>

                    Aktivna trenerica za različite edukativne/trening programe u poslednjih 17 godina. Sertifikovani je
                    kouč
                    za organizacioni kognitivno-bihejvioralni koučing. Aktuelno na završnom nivou edukacije za
                    psihoterapeuta REBT orijentacije u okviru pridruženog trening centar Instituta Albert Elis iz
                    Njujorka.
                    <br> <br>

                    Majka dvoje dece, aktivno trenira pilates preko 10 godina, spremna na timski rad u poslu i životu,
                    voli
                    putovanja, muziku i dobre žurke.<br>

                </p>
                <p></p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-4">
                <div class="team_image">
                    <img src="<?php echo get_template_directory_uri()?>/img/team3.png" alt="Team">
                </div>
            </div>
            <div class="col-12 col-sm-8 team_content">
                <h2 class="section-title mb-normal"><span class="mark"></span>O NAMA</h2>
                <p class="subtitle">CEO OF PURE ENGINE</p>
                <p class="more">

                    Završila osnovne studije psihologije na Filozofskom fakultetu Univerziteta u Novom Sadu, kao i
                    specijalističke jednogodišnje studije iz oblasti poslovne psihologije. Master studije završila je na
                    Fakultetu za pravne i poslovne studije „Dr. Lazar Vrkatić“ na studijskom programu poslovna
                    psihologija.
                    <br>

                    U poslednje 4 godine radi na poziciji direktorke Centra za porodični smeštaj i usvojenje Novi Sad.
                    Prethodno radno iskustvo je dominantno u oblasti socijalne zaštite imala kroz rad na poslovima
                    savetnice
                    u Pokrajinskom zavodu za socijalnu zaštitu, kao i kroz rad u civilnom sektoru.<br>

                    Pripremala je i realizovala brojne domaće i međunarodne projekte. Koautorka i realizatorka je
                    nekoliko
                    akreditovanih programa za stručno usavršavanje zaposlenih u sistemu socijalne zaštite. Autorka je
                    nekoliko priručnika i stručnih publikacija. Realizovala je obuke i za zaposlene u IT sektoru. <br>

                    Aktivna trenerica za različite edukativne/trening programe u poslednjih 17 godina. Sertifikovani je
                    kouč
                    za organizacioni kognitivno-bihejvioralni koučing. Aktuelno na završnom nivou edukacije za
                    psihoterapeuta REBT orijentacije u okviru pridruženog trening centar Instituta Albert Elis iz
                    Njujorka.
                    <br>

                    Majka dvoje dece, aktivno trenira pilates preko 10 godina, spremna na timski rad u poslu i životu,
                    voli
                    putovanja, muziku i dobre žurke.<br>

                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-4">
                <div class="team_image">
                    <img src="<?php echo get_template_directory_uri()?>/img/team1.png" alt="Team">
                </div>
            </div>
            <div class="col-12 col-sm-8 team_content">
                <h2 class="section-title mb-normal"><span class="mark"></span>O NAMA</h2>
                <p class="subtitle">CEO OF PURE ENGINE</p>

                <p class="more">

                    Završila osnovne studije psihologije na Filozofskom fakultetu Univerziteta u Novom Sadu, kao i
                    specijalističke jednogodišnje studije iz oblasti poslovne psihologije. Master studije završila je na
                    Fakultetu za pravne i poslovne studije „Dr. Lazar Vrkatić“ na studijskom programu poslovna
                    psihologija.
                    <br>

                    U poslednje 4 godine radi na poziciji direktorke Centra za porodični smeštaj i usvojenje Novi Sad.
                    Prethodno radno iskustvo je dominantno u oblasti socijalne zaštite imala kroz rad na poslovima
                    savetnice
                    u Pokrajinskom zavodu za socijalnu zaštitu, kao i kroz rad u civilnom sektoru.<br>

                    Pripremala je i realizovala brojne domaće i međunarodne projekte. Koautorka i realizatorka je
                    nekoliko
                    akreditovanih programa za stručno usavršavanje zaposlenih u sistemu socijalne zaštite. Autorka je
                    nekoliko priručnika i stručnih publikacija. Realizovala je obuke i za zaposlene u IT sektoru. <br>

                    Aktivna trenerica za različite edukativne/trening programe u poslednjih 17 godina. Sertifikovani je
                    kouč
                    za organizacioni kognitivno-bihejvioralni koučing. Aktuelno na završnom nivou edukacije za
                    psihoterapeuta REBT orijentacije u okviru pridruženog trening centar Instituta Albert Elis iz
                    Njujorka.
                    <br>

                    Majka dvoje dece, aktivno trenira pilates preko 10 godina, spremna na timski rad u poslu i životu,
                    voli
                    putovanja, muziku i dobre žurke.<br>

                </p>
            </div>
        </div>

    </div>
</section>

<section class="background-green cta-box">
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-12 col-sm-3 img">
                <img src="<?php echo get_template_directory_uri()?>/img/group_letters.svg">
            </div>
            <div class="col-12 col-sm-6">
                <h3>IMATE PITANJE ZA NAS?</h3>
                <p>Slobodno nas kontaktiraje</p>
                <a href="#" class="btn-full btn-dark btn-arrow">Pišite nam</a>
            </div>
        </div>
    </div>
</section>


<?php get_footer(); ?>