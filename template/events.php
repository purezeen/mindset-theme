<?php /* Template Name: Events */ get_header(); ?>

<section class="hero cover"
   style="background-image:linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%) , url(<?php echo get_template_directory_uri()?>/img/about-hero.png)">
</section>

<section class="section curve-white curve-padding-top">
   <div class="container">

      <div class="row">
         <div class="col-12 text-center">
            <h1 class="section-title mb-big"><span class="mark"></span>KALENDAR DOGAĐAJA</h1>
         </div>
      </div>

      <a href="/events/auto-draft/" class="event-promo">
         <div class="event">
            <div class="event-header">
               <div class="event_data">
                  <div class="big">
                     <p> 29</p> <span><br>FEB</span>
                  </div>
                  <div class="small">
                     <em>-</em> 26<span><br>JUL</span>
                  </div>
               </div>
               <div class="event_type">predavanje</div>
            </div>
            <div class="event_content">
               <h4>MULTI DAY EVENT</h4>
               <p>6:00 Pm - 10:00 Pm (22)</p>
               <p>Irving City Park, NE Fremont St, Portland, OR 97212</p>
            </div>
         </div>
         <div class="event-promo_image cover" style="background-image:url(<?php echo get_template_directory_uri()?>/img/event-promo.png)">
         </div>
      </a>

      <div class="event-list relative">

         <a href="/events/auto-draft/" class="event">
            <div class="event-header">
               <div class="event_data">
                  <div class="big">
                     <p> 16</p> <span><br>JUL</span>
                  </div>
                  <div class="small">
                     <em>-</em> 26<span><br>JUL</span>
                  </div>
               </div>
               <div class="event_type">predavanje</div>
            </div>
            <div class="event_content">
               <h4>SLIDEDOWN EVENT</h4>
               <p>(July 16) 6:00 Am - (September 26) 9:00 Am</p>
               <p>Irving City Park, NE Fremont St, Portland, OR 97212</p>
            </div>
         </a>

         <a href="/events/auto-draft/" class="event event_mark">
            <div class="event-header">
               <div class="event_data">
                  <div class="big">
                     <p> 16</p><span><br>JUL</span>
                  </div>
                  <div class="small"><em>-</em> 26<span><br>JUL</span></div>
               </div>
            </div>
            <div class="event_content">
               <h4>EVENT WITH FEATURED IMAGE</h4>
               <p>4:00 Pm - 10:00 Pm</p>
               <p>Portland Bridges, 400 Southwest Kingston Avenue Portland, OR</p>
            </div>
         </a>

         <a href="/events/auto-draft/" class="event">
            <div class="event-header">
               <div class="event_data">
                  <div class="big">
                     <p> 16</p> <span><br>JUL</span>
                  </div>
                  <div class="small">
                     <em>-</em> 26<span><br>JUL</span>
                  </div>
               </div>
               <div class="event_type">predavanje</div>
            </div>
            <div class="event_content">
               <h4>SLIDEDOWN EVENT</h4>
               <p>(July 16) 6:00 Am - (September 26) 9:00 Am</p>
               <p>Irving City Park, NE Fremont St, Portland, OR 97212</p>
            </div>
         </a>

         <a href="/events/auto-draft/" class="event event_mark">
            <div class="event-header">
               <div class="event_data">
                  <div class="big">
                     <p> 16</p> <span><br>JUL</span>
                  </div>
                  <div class="small"><em>-</em> 26<span><br>JUL</span></div>
               </div>
            </div>
            <div class="event_content">
               <h4>EVENT WITH FEATURED IMAGE</h4>
               <p>4:00 Pm - 10:00 Pm</p>
               <p>Portland Bridges, 400 Southwest Kingston Avenue Portland, OR</p>
            </div>
         </a>

      </div>

      <div class="pagination center">
			<ul class="unstyle-list">
				<li class="prev"></li>
				<span class="pagination-number">01/02</span>
				<li class="next"></li>
			</ul>
		</div>
   </div>
</section>

<section class="background-green cta-box">
   <div class="container">
         <div class="row justify-content-end">
               <img src="<?php echo get_template_directory_uri()?>/img/group_letters.svg">
            </div>
            <div class="col-12 col-sm-6">
               <h3>IMATE PITANJE ZA NAS?</h3>
               <p>Slobodno nas kontaktiraje</p>
               <a href="#" class="btn-full btn-dark btn-arrow">Pišite nam</a>
            </div>
         </div>
   </div>
</section>


<?php get_footer(); ?>