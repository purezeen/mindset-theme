<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gulp-wordpress
 */

get_header(); ?>

<div id="primary" class="content-area container">
	<main id="main" class="site-main row" role="main">

		<div class="post-hero border-shadow col-12">
			<img src="<?php echo get_template_directory_uri()?>/img/post-hero.png" alt="">
		</div>

		<div class="post-header col-12">
			<h1>Tajne neverbalne komunikacije </h1>
			<p class="subtitle">kako ovladati ovom komunikacionom mrežom?</p>
			<hr>
		</div>

		<div class="col-12 editor">
			<?php		
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', get_post_format() );

			endwhile; // End of the loop.
			?>

		</div>
	</main><!-- #main -->
</div><!-- #primary -->

<section class="post-author background-grey">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-12 col-md-3 col-lg-2">
				<div class="post-author_image">
					<img src="<?php echo get_template_directory_uri()?>/img/o-nama.png" alt="">
				</div>
			</div>
			<div class="col-12 col-md-7 col-lg-10 post-author_content">
				<span>O AUTORU</span>
				<h3>Liza Piščević</h3>
				<p>Liza Piščević je master psiholog na Filozofskom Fakultetu, na katedri za psihologiju obrazovanja.
					Sertifikovani je trener asertivne komunikacije sa diplomama online kurseva Stanford Univerziteta.
				</p>
			</div>
			<div class="col-12">
				<div class="clearfix" id="share">
					<?php echo do_shortcode('[addtoany]');  ?>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="services-slider section">

	<div class="row">
		<div class="col-12 d-flex justify-content-center">
			<h2 class="section-title mb-big"><span class="mark"></span>Usluge</h2>
		</div>
	</div>

	<div class="center">
		<div class="container">
			<a class="row justify-content-center">
					<div class="col-12 col-sm-6">
						<div class="services_content">
							<h3>KLASICNE OBUKE</h3>
							<p class="subtitle">Trajanje: <strong>90-120 min</strong> Broj: <strong>preko
										25</strong></p>
							<p>Nasa predavanja mogu da se prilagode vasim potrebama, mogu se odrzati kod vas ili u
									nekom drugom
									okruzenju koje vi izaberete za svoje zaposlene. Mogu biti u toku ili nakon radnog
									vremena, a mozda i
									vikendom...</p>
							<span class="btn-link btn-link_green">Saznaj više</span>
						</div>
					</div>
					<div class="col-0 col-sm-6 services_image">
						<div class="services_shapes">
							<div class="services_circle"
									style="background-image: linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%), url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
							</div>
							<div class="circle-bigger"></div>
							<div class="circle-small"></div>
						</div>
					</div>
			</a>
		</div>
		<div class="container">
			<a class="row justify-content-center">
					<div class="col-12 col-sm-6">
						<div class="services_content">
							<h3>KLASICNE OBUKE</h3>
							<p class="subtitle">Trajanje: <strong>90-120 min</strong> Broj: <strong>preko
										25</strong></p>
							<p>Nasa predavanja mogu da se prilagode vasim potrebama, mogu se odrzati kod vas ili u
									nekom drugom
									okruzenju koje vi izaberete za svoje zaposlene. Mogu biti u toku ili nakon radnog
									vremena, a mozda i
									vikendom...</p>
							<span class="btn-link btn-link_green">Saznaj više</span>
						</div>
					</div>
					<div class="col-0 col-sm-6 services_image">
						<div class="services_shapes">
							<div class="services_circle"
									style="background-image: linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%), url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
							</div>
							<div class="circle-bigger"></div>
							<div class="circle-small"></div>
						</div>
					</div>
			</a>
		</div>
		<div class="container">
			<a class="row justify-content-center">
					<div class="col-12 col-sm-6">
						<div class="services_content">
							<h3>KLASICNE OBUKE</h3>
							<p class="subtitle">Trajanje: <strong>90-120 min</strong> Broj: <strong>preko
										25</strong></p>
							<p>Nasa predavanja mogu da se prilagode vasim potrebama, mogu se odrzati kod vas ili u
									nekom drugom
									okruzenju koje vi izaberete za svoje zaposlene. Mogu biti u toku ili nakon radnog
									vremena, a mozda i
									vikendom...</p>
							<span class="btn-link btn-link_green">Saznaj više</span>
						</div>
					</div>
					<div class="col-0 col-sm-6 services_image">
						<div class="services_shapes">
							<div class="services_circle"
									style="background-image: linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%), url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
							</div>
							<div class="circle-bigger"></div>
							<div class="circle-small"></div>
						</div>
					</div>
			</a>
		</div>
	</div>

	<div class="paginator center text-color text-center">
		<ul class="unstyle-list">
			<li class="prev"></li>
			<span class="pagingInfo"></span>
			<li class="next"></li>
		</ul>
	</div>
</section>

<section class="latest-post container section">
	<a>
		<div class="latest-post_image cover"
			style="background-image:url(<?php echo get_template_directory_uri()?>/img/blog1.png)">
		</div>
		<div class="post-content">
			<h5>Neverbalna komunikacija</h5>
			<p class="subtitle">07.04.1019. &nbsp; | &nbsp; <span>Kurs</span></p>
			<p>Iako mi se čini izuzetno dualistički,  tamu, ovi opaženi kvaliteti
				postoje kao takvi, ako ne kroz perspektivu apsoluta, onda kao kreacija ljudskog uma, sa ciljem
				razumevanja i raslojavanja iskustva, vrlo...</p>

			<span href="" class="btn-link btn-link_green">Saznaj više</span>
		</div>
	</a>
	<a>
		<div class="latest-post_image cover"
			style="background-image:url(<?php echo get_template_directory_uri()?>/img/blog2.png)">
		</div>
		<div class="post-content">
			<h5>Neverbalna komunikacija</h5>
			<p class="subtitle">07.04.1019. &nbsp; | &nbsp; <span>Kurs</span></p>
			<p>Iako mi se čini izuzetno dualistički, da stvarnost segmentiramo na svetlost i tamu, ovi opaženi kvaliteti
				postoje kao takvi, ako ne kroz perspektivu apsoluta, onda kao kreacija ljudskog uma, sa ciljem
				razumevanja i raslojavanja iskustva, vrlo...</p>

			<span href="" class="btn-link btn-link_green">Saznaj više</span>
		</div>
	</a>
	<a>
		<div class="latest-post_image cover"
			style="background-image:url(<?php echo get_template_directory_uri()?>/img/blog3.png)">
		</div>
		<div class="post-content">
			<h5>Neverbalna komunikacija</h5>
			<p class="subtitle">07.04.1019. &nbsp; | &nbsp; <span>Kurs</span></p>
			<p>Iako mi se čini izuzetno dualistički, da stvarnost segmentiramo na svetlost i tamu, ovi opaženi kvaliteti
				razumevanja i raslojavanja iskustva, vrlo...</p>

			<span href="" class="btn-link btn-link_green">Saznaj više</span>
		</div>
	</a>
</section>

<section class="background-green cta-box">
	<div class="container">
			<div class="row justify-content-end">
				<div class="col-12 col-sm-3 img">
					<img src="<?php echo get_template_directory_uri()?>/img/group_letters.svg">
				</div>
				<div class="col-12 col-sm-6">
					<h3>IMATE PITANJE ZA NAS?</h3>
					<p>Slobodno nas kontaktiraje</p>
					<a href="#" class="btn-full btn-dark btn-arrow">Pišite nam</a>
				</div>
			</div>
	</div>
</section>

<?php
get_footer();