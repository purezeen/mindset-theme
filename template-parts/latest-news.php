<!-- latest news -->
<section class="container latest-news section">
      <h2 class="text-center section-title">LATEST NEWS</h2>
      <?php
      $currentID = get_the_ID();
      $the_query = new WP_Query( array(
         'posts_per_page' => 2,
         'post__not_in' => array($currentID),
      )); 
      if ( $the_query->have_posts() ) : ?>
         <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
            <h2>post</h2>
            <?php get_template_part( 'template-parts/content', 'page' ); ?>

         <?php endwhile; ?>
         <?php wp_reset_postdata(); ?>

         <?php else : ?>
         <p><?php __('No News'); ?></p>
      <?php endif; ?>

      <div class="row">
         <a href="/blog" class="btn btn-dark btn-center">
            <span>Vise</span>
         </a>
      </div>
</section>
<!-- end latest news -->