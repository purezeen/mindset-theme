<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gulp-wordpress
 */

?>

<a href="<?php echo get_permalink();?>" id="post-<?php the_ID(); ?>" <?php post_class('row horizontal-cart margin-b'); ?> href="<?php echo get_permalink();?>">
	<?php
		if (has_post_thumbnail()) {
		$backgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'thumbnail'); 
		}
	?>
	<div class="col-3 col-md-6 cover horizontal-shape" style="background-image: url('<?php echo $backgroundImg[0]?>')">

	</div>

	<div class="col-9 col-md-6 inner-content">
		<span class="color-light-red"><?php the_field( 'start_date' ); ?> : <?php the_field( 'end_date' ); ?></span>
		<h5 class="uppercase color-red"><?php the_title(); ?> </h5>
		<p><?php the_excerpt(); ?> </p>
		<div class="btn-link">Geschichte</div>
	</div>

</a><!-- #post-## -->
