<?php /* Template Name: Pocetna */ 

get_header(); ?>

    <main id="main" class="site-main" role="main">

        <section class="hero background-green">
            <img class="shape1" src="<?php echo get_template_directory_uri()?>/img/background-shape1.svg" alt="">
            <img class="shape2" src="<?php echo get_template_directory_uri()?>/img/background-shape2.svg" alt="">
            <h1>For those with the <br> <strong>growth</strong> mindset</h1>
        </section>

        <section class="section curve-white curve-padding-top curve-padding-bottom">
            <div class="container">
                <div class="row two-columns align-items-center">
                    <div class="col-12 col-md-6">
                        <div class="circle-shapes">
                            <div class="big-circle cover"
                                style="background-image: linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%), url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
                            </div>
                            <div class="circle-bigger "></div>
                            <div class="circle-big"></div>
                            <div class="circle-small"></div>
                        </div>

                    </div>
                    <div class="col-12 col-md-6">
                        <div class="line-left box-content">
                            <h2 class="section-title mb-normal"><span class="mark"></span>O NAMA</h2>
                            <p>Centar za lični i profesionalni razvoj “Mindset”, osnovan je 2019. godine, kako bismo
                                primenili svoje
                                znanje i iskustvo i pružili podršku svima koji žele da rastu.</p>
                            <p>Mi znamo da iako svi nemamo iste talente i sposobnosti, ipak možemo da ih razvijamo tokom
                                celog života.
                                Jednako kao što se mišići mogu ojačati vežbanjem u teretani, sposobnosti i talenti se
                                mogu
                                razvijati i
                                unaprediti kroz adekvatnu ekspertsku podršku i prilagođene razvojne zadatke.</p>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <section class="background-grey section curve-grey curve-padding-top mind-box-section">
            <div class="container">
                <div class="row">
                    <div class="col-12 d-flex justify-content-center">
                        <h2 class="section-title mb-big"><span class="mark"></span>PROCENI SVOJ MINDSET</h2>
                    </div>
                </div>
                <div class="row two-columns mind-box">
                    <div class="col-12 col-md-6">
                        <div class="box-content border">
                            <div class="mind-img">
                                <img src="<?php echo get_template_directory_uri()?>/img/growth-mindset.png"
                                    alt="Growth mindset">
                            </div>
                            <h3><span class="color-green">GROWTH</span><br> MINDSET</h3>
                            <ul>
                                <li>Mogu da naučim sve što želim,</li>
                                <li>Kada naiđem na teškoće, uporan sam</li>
                                <li>Želim da se suočavam sa izazovima</li>
                                <li>Ako ne uspem, nešto ću naučiti</li>
                                <li>Reci mi da se trudim i zalažem</li>
                                <li>Uspeh dugih me motiviše</li>
                                <li>Moj trud i stav određuju postignuće</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="box-content border relative">
                            <div class="mind-img">
                                <img src="<?php echo get_template_directory_uri()?>/img/fixed-mindset.png"
                                    alt="Fixed mindset">
                            </div>
                            <h3><span class="color-green">FIXED</span><br> MINDSET</h3>
                            <ul>

                                <li>
                                    Ili sam u nečemu dobar ili nisam
                                </li>
                                <li>
                                    Kada naiđem na teškoće, odustajem
                                </li>
                                <li>
                                    Ne volim suočavanje sa izazovima
                                </li>
                                <li>
                                    Ako ne uspem, znači da ne valjam
                                </li>
                                <li>
                                    Reci mi da sam pametan
                                </li>
                                <li>
                                    Uspeh drugih me ugrožava
                                </li>
                                <li> Moje sposobnosti određuju postignuće</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="background-green cta-box">
            <div class="container">
                <div class="row justify-content-end">
                    <div class="col-12 col-sm-3 img">
                        <img src="<?php echo get_template_directory_uri()?>/img/group_letters.svg">
                    </div>
                    <div class="col-12 col-sm-6">
                        <h3>IMATE PITANJE ZA NAS?</h3>
                        <p>Slobodno nas kontaktiraje</p>
                        <a href="#" class="btn-full btn-dark btn-arrow">Pišite nam</a>
                    </div>
                </div>
            </div>
        </section>

        <section class="services-slider section curve-padding-bottom">

            <div class="row">
                <div class="col-12 d-flex justify-content-center">
                    <h2 class="section-title mb-big"><span class="mark"></span>Usluge</h2>
                </div>
            </div>

            <div class="center">
                <div class="container">
                    <a class="row justify-content-center">
                        <div class="col-12 col-sm-6">
                            <div class="services_content">
                                <h3>KLASICNE OBUKE</h3>
                                <p class="subtitle">Trajanje: <strong>90-120 min</strong> &nbsp;	| &nbsp; Broj: <strong>preko
                                        25</strong></p>
                                <p>Nasa predavanja mogu da se prilagode vasim potrebama, mogu se odrzati kod vas ili u
                                    nekom drugom
                                    okruzenju koje vi izaberete za svoje zaposlene. Mogu biti u toku ili nakon radnog
                                    vremena, a mozda i
                                    vikendom...</p>
                                <span class="btn-link btn-link_green">Saznaj više</span>
                            </div>
                        </div>
                        <div class="col-0 col-sm-6 services_image">
                            <div class="services_shapes">
                                <div class="services_circle"
                                    style="background-image: linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%), url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
                                </div>
                                <div class="circle-bigger"></div>
                                <div class="circle-small"></div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="container">
                    <a class="row justify-content-center">
                        <div class="col-12 col-sm-6">
                            <div class="services_content">
                                <h3>KLASICNE OBUKE</h3>
                                <p class="subtitle">Trajanje: <strong>90-120 min</strong> Broj: <strong>preko
                                        25</strong></p>
                                <p>Nasa predavanja mogu da se prilagode vasim potrebama, mogu se odrzati kod vas ili u
                                    nekom drugom
                                    okruzenju koje vi izaberete za svoje zaposlene. Mogu biti u toku ili nakon radnog
                                    vremena, a mozda i
                                    vikendom...</p>
                                <span class="btn-link btn-link_green">Saznaj više</span>
                            </div>
                        </div>
                        <div class="col-0 col-sm-6 services_image">
                            <div class="services_shapes">
                                <div class="services_circle"
                                    style="background-image: linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%), url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
                                </div>
                                <div class="circle-bigger"></div>
                                <div class="circle-small"></div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="container">
                    <a class="row justify-content-center">
                        <div class="col-12 col-sm-6">
                            <div class="services_content">
                                <h3>KLASICNE OBUKE</h3>
                                <p class="subtitle">Trajanje: <strong>90-120 min</strong> &nbsp;	| &nbsp; Broj: <strong>preko
                                        25</strong></p>
                                <p>Nasa predavanja mogu da se prilagode vasim potrebama, mogu se odrzati kod vas ili u
                                    nekom drugom
                                    okruzenju koje vi izaberete za svoje zaposlene. Mogu biti u toku ili nakon radnog
                                    vremena, a mozda i
                                    vikendom...</p>
                                <span class="btn-link btn-link_green">Saznaj više</span>
                            </div>
                        </div>
                        <div class="col-0 col-sm-6 services_image">
                            <div class="services_shapes">
                                <div class="services_circle"
                                    style="background-image: linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%), url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
                                </div>
                                <div class="circle-bigger"></div>
                                <div class="circle-small"></div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="paginator center text-color text-center">
                <ul class="unstyle-list">
                    <li class="prev"></li>
                    <span class="pagingInfo"></span>
                    <li class="next"></li>
                </ul>
            </div>
        </section>

        <section class="section background-grey curve-grey curve-padding-top">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1 class="section-title mb-big"><span class="mark"></span>KALENDAR DOGAĐAJA</h1>
                    </div>
                </div>

                <div class="event-list relative">

                    <a class="event" href="#events/auto-draft/">
                        <div class="event-header">
                            <div class="event_data">
                                <div class="big">
                                    <p> 16</p> <span><br>JUL</span>
                                </div>
                                <div class="small">
                                    <em>-</em> 26<span><br>JUL</span>
                                </div>
                            </div>
                            <div class="event_type">predavanje</div>
                        </div>
                        <div class="event_content">
                            <h4>SLIDEDOWN EVENT</h4>
                            <p class="icon-time-grey">(July 16) 6:00 Am - (September 26) 9:00 Am</p>
                            <p class="icon-location-grey">Irving City Park, NE Fremont St, Portland, OR 97212</p>
                        </div>
                    </a>

                    <a class="event event_mark" href="#events/auto-draft/">
                        <div class="event-header">
                            <div class="event_data">
                                <div class="big">
                                    <p> 16</p><span><br>JUL</span>
                                </div>
                                <div class="small"><em>-</em> 26<span><br>JUL</span></div>
                            </div>
                        </div>
                        <div class="event_content">
                            <h4>EVENT WITH FEATURED IMAGE</h4>
                            <p>4:00 Pm - 10:00 Pm</p>
                            <p>Portland Bridges, 400 Southwest Kingston Avenue Portland, OR</p>
                        </div>
                    </a>

                    <a class="event" href="#events/auto-draft/">
                        <div class="event-header">
                            <div class="event_data">
                                <div class="big">
                                    <p> 16</p> <span><br>JUL</span>
                                </div>
                                <div class="small">
                                    <em>-</em> 26<span><br>JUL</span>
                                </div>
                            </div>
                            <div class="event_type">predavanje</div>
                        </div>
                        <div class="event_content">
                            <h4>MULTI DAY EVENT</h4>
                            <p>(July 16) 6:00 Am - (September 26) 9:00 Am</p>
                            <p>Irving City Park, NE Fremont St, Portland, OR 972122</p>
                        </div>
                    </a>

                </div>

                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <a href="" class="btn-full btn-light-green btn-center">Ostali događaji</a>
                    </div>
                </div>


            </div>
        </section>

        <section class="testimonial section">
            <img class="shape1" src="<?php echo get_template_directory_uri()?>/img/background-shape1.svg" alt="">
            <img class="shape2" src="<?php echo get_template_directory_uri()?>/img/background-shape2.svg" alt="">
            <div class="container">
                <div class="row">
                    <div class="col-12 d-flex justify-content-center">
                        <h2 class="section-title mb-big"><span class="mark"></span>Preporuke</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 testimonial_wrap">
                        <div class="fade">
                            <div>
                                <div class="testimonial_box">
                                    <blockquote>
                                        pLorem Ipsum is simply dummy text of the printing and typesetting industry.
                                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                        when an unknown printer took a galley of type and scrambled it to make a type
                                        specimen book.
                                    </blockquote>

                                    <div class="testimonial_author">
                                        <div>
                                            <h6>Katarina Stepanović</h6>
                                            <p>CEO of Pure Engine</p>
                                        </div>
                                        <div class="testimonial_img"
                                            style="background-image: url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <div class="testimonial_box">
                                    <blockquote>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit tempora, ad
                                        quisquam temporibus omnis dolor repudiandae tenetur praesentium fugiat deleniti
                                        neque veniam eius officiis! Harum natus.
                                    </blockquote>

                                    <div class="testimonial_author">
                                        <div>
                                            <h6>Tijana Radakovic</h6>
                                            <p>CEO of Pure Engine</p>
                                        </div>
                                        <div class="testimonial_img"
                                            style="background-image: url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="paginator center text-color text-center">
                            <ul class="unstyle-list">
                                <li class="prev"></li>
                                <li class="next"></li>
                            </ul>
                        </div>
        </section>

        <section class="container section team-home">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-7 text-center team-home_title_box">
                    <h2 class="section-title mb-normal center"><span class="mark"></span>NAŠ TIM</h2>
                    <p class="mb-big">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                        Ipsum has been the industry's </p>
                </div>
            </div>

            <div class="team-home_wrap">
   
                <a class="team-home_box">
                    <div class="team-home_img">
                        <img src="<?php echo get_template_directory_uri()?>/img/team1.png" alt="Team">
                    </div>
                    <div class="team-home_content">
                    <hr>
                        <h6>Ivana Koprivica</h6>
                        <p>CEO of Pure Engine</p>
                    </div>
                </a>
                <a class="team-home_box">
                    <div class="team-home_img">
                        <img src="<?php echo get_template_directory_uri()?>/img/team2.png" alt="Team">
                    </div>
                    <div class="team-home_content">
                        <hr>
                        <h6>Marina Vukotić</h6>
                        <p>CEO of Pure Engine</p>
                    </div>
                </a>
                <a class="team-home_box">
                    <div class="team-home_img">
                        <img src="<?php echo get_template_directory_uri()?>/img/team3.png" alt="Team">
                    </div>
                    <div class="team-home_content">
                        <hr>
                        <h6>Tatjana Lazor Obradović</h6>
                        <p>CEO of Pure Engine</p>
                    </div>
                </a>
                <a class="team-home_box">
                    <div class="team-home_img">
                        <img src="<?php echo get_template_directory_uri()?>/img/team4.png" alt="Team">
                    </div>
                    <div class="team-home_content">
                        <hr>
                        <h6>Mirjana Beara</h6>
                        <p>CEO of Pure Engine</p>
                    </div>
                </a>
                             
            </div>

                <div class="d-flex">
                    <a href="`3" class="btn-full btn-light-green btn-center">Upoznajte tim</a>
                </div>

            </div>
        </section>
    </main><!-- #main -->

<?php
get_footer();