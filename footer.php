<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gulp-wordpress
 */

?>

	</div><!-- #content -->

	<h1 class="scroll"><img src="<?php echo get_template_directory_uri()?>/img/scroll-top.svg" alt="Mindset"></h1>

	<footer id="colophon" class="site-footer background-grey" role="contentinfo">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-12 col-md-4"><img src="<?php echo get_template_directory_uri()?>/img/mindset-logo2.svg" alt="Mindset">© 2019
				</div>
				<div class="col-12 col-md-4 text-center">
					<p>Say <strong>hi@mindset.com</strong></p>
				</div>
				<div class="col-12 col-md-4 social-icon">
					<div><span class="iconify" data-icon="mdi:facebook" data-inline="false"></span></div>
					<div><span class="iconify" data-icon="entypo-social:linkedin" data-inline="false"></span></div>
					<div><span class="iconify" data-icon="ant-design:instagram-outline" data-inline="false"></span></div>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->


<div class="container-fluid contact modal md-effect-12">
	<div class="contact_close"><span>+ </span></div>
	<div class="row">
		<div class="col-12 col-md-7 col-lg-6 contact_info">
			<div>
				<div class="section-form">
					<div class="section-form_header">
						<h5>Imate pitanje za nas?</h5>
						<p>Slobodno nas kontaktiraje</p>
					</div>
					<?php echo do_shortcode('[contact-form-7 id="68" title="Kontakt"]') ?>
				</div>
				<div class="contact_info_inner">
					<p class="icon-email">hi@mindset.com</p>
					<p class="icon-location">Mihaijla Pupina 21,<br>21 000 Novi Sad</p>
					<a href="#" class="btn-link btn-link_light_green">POGLEDAJ NA MAPI</a>
					<div class="social-icon">
						<div><span class="iconify" data-icon="mdi:facebook" data-inline="false"></span></div>
						<div><span class="iconify" data-icon="entypo-social:linkedin" data-inline="false"></span></div>
						<div><span class="iconify" data-icon="ant-design:instagram-outline" data-inline="false"></span></div>
					</div>
				</div>
			</div>
			
			<img class="shape1" src="<?php echo get_template_directory_uri()?>/img/background-shape1.svg" alt="">
		</div>
		<div class="col-12 col-md-5 col-lg-6 contact_map">
			<div id='map' class='map'> </div>
		</div>
	</div>
</div>


<?php wp_footer(); ?>

</body>
</html>