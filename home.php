<?php /* Template Name: Blog */ 

get_header(); ?>

<section class="hero cover" style="background-image:linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%) , url(<?php echo get_template_directory_uri()?>/img/blog-hero.png)">
</section>
	
	<section class="blog-page curve-white section curve-padding-top">
		<div class="container">

			<div class=" text-center">
				<h1 class="section-title mb-big"><span class="mark"></span>BLOG</h1>
			</div>
			<a class="row" href="/hello-world/">
				<div class="col-12 col-sm-4 col-md-6">	
					<div class="blog-page_image cover" style="background-image:linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%) , url(<?php echo get_template_directory_uri()?>/img/blog1.png)">
					</div>
				</div>
				<div class="col-12 col-sm-8 col-md-6 d-flex align-items-center">
					<div class="blog-page_content">
						<h3>NEVERBALNA KOMUNIKACIJA</h3>
						<p class="subtitle">22.07. - 13.08.2019. &nbsp;	| &nbsp; <span>Radionica</span></p>

						<p>Naša predavanja mogu da se prilagode vašim potrebama, mogu se održati kod vas ili u nekom drugom okruženju koje vi izaberete za svoje zaposlene.  Mogu biti u toku ili nakon radnog vremena, a možda i vikendom…</p>

						<span href="" class="btn-link btn-link_green">Saznaj više</span>
					</div>
				</div>
			</a>

			<a class="row" href="/hello-world/">
				<div class="col-12 col-sm-4 col-md-6">	
					<div class="blog-page_image cover" style="background-image:linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%) , url(<?php echo get_template_directory_uri()?>/img/blog2.png)">
					</div>
				</div>
				<div class="col-12 col-sm-8 col-md-6 d-flex align-items-center">
					<div class="blog-page_content">
						<h3>NEVERBALNA KOMUNIKACIJA</h3>
						<p class="subtitle">22.07. - 13.08.2019. &nbsp;	| &nbsp; <span>Radionica</span></p>

						<p>Naša predavanja mogu da se prilagode vašim potrebama, mogu se održati kod vas ili u nekom drugom okruženju koje vi izaberete za svoje zaposlene.  Mogu biti u toku ili nakon radnog vremena, a možda i vikendom…</p>

						<span href="#" class="btn-link btn-link_green">Saznaj više</span>
					</div>
				</div>
			</a>

			<a class="row" href="/hello-world/">
				<div class="col-12 col-sm-4 col-md-6">	
					<div class="blog-page_image cover" style="background-image:linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%) , url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
					</div>
				</div>
				<div class="col-12 col-sm-8 col-md-6 d-flex align-items-center">
					<div class="blog-page_content">
						<h3>Detoks organizma</h3>
						<p class="subtitle">23.05.2019. &nbsp;	| &nbsp; <span>Obuka, Kurs</span></p>

						<p>Za sve vas koji volite da se čistite, uživate u zdravim izborima i posebno ste srećni kada radite nešto korisno za sebe, nastavite da čitate, jer sledi još jedna, veoma jednostavna ideja, koja vam dalje može u značajnoj meri pomoći…</p>

						<span href="" class="btn-link btn-link_green">Saznaj više</span>
					</div>
				</div>
			</a>

			<a class="row" href="/hello-world/">
				<div class="col-12 col-sm-4 col-md-6">	
					<div class="blog-page_image cover" style="background-image:linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%) , url(<?php echo get_template_directory_uri()?>/img/blog3.png)">
					</div>
				</div>
				<div class="col-12 col-sm-8 col-md-6 d-flex align-items-center">
					<div class="blog-page_content">
						<h3>NEVERBALNA KOMUNIKACIJA</h3>
						<p class="subtitle">22.07. - 13.08.2019. &nbsp;	| &nbsp; <span>Radionica</span></p>

						<p>Iako mi se čini izuzetno dualistički, da stvarnost segmentiramo na svetlost i tamu, ovi opaženi kvaliteti postoje kao takvi, ako ne kroz perspektivu apsoluta, onda kao kreacija ljudskog uma, sa ciljem razumevanja i raslojavanja iskustva, vrlo...</p>

						<span href="" class="btn-link btn-link_green">Saznaj više</span>
					</div>
				</div>
			</a>

			<a class="row" href="/hello-world/">
				<div class="col-12 col-sm-4 col-md-6">	
					<div class="blog-page_image cover" style="background-image:linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%) , url(<?php echo get_template_directory_uri()?>/img/o-nama.png)">
					</div>
				</div>
				<div class="col-12 col-sm-8 col-md-6 d-flex align-items-center">
					<div class="blog-page_content">
						<h3>Detoks organizma</h3>
						<p class="subtitle">23.05.2019. &nbsp;	| &nbsp; <span>Obuka, Kurs</span></p>

						<p>Za sve vas koji volite da se čistite, uživate u zdravim izborima i posebno ste srećni kada radite nešto korisno za sebe, nastavite da čitate, jer sledi još jedna, veoma jednostavna ideja, koja vam dalje može u značajnoj meri pomoći…</p>

						<span href="" class="btn-link btn-link_green">Saznaj više</span>
					</div>
				</div>
			</a>

			<a class="row">
				<div class="col-12 col-sm-4 col-md-6">	
					<div class="blog-page_image cover" style="background-image:linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%) , url(<?php echo get_template_directory_uri()?>/img/blog3.png)">
					</div>
				</div>
				<div class="col-12 col-sm-8 col-md-6 d-flex align-items-center">
					<div class="blog-page_content">
						<h3>NEVERBALNA KOMUNIKACIJA</h3>
						<p class="subtitle">22.07. - 13.08.2019. &nbsp;	| &nbsp; <span>Radionica</span></p>

						<p>Iako mi se čini izuzetno dualistički, da stvarnost segmentiramo na svetlost i tamu, ovi opaženi kvaliteti postoje kao takvi, ako ne kroz perspektivu apsoluta, onda kao kreacija ljudskog uma, sa ciljem razumevanja i raslojavanja iskustva, vrlo...</p>

						<span href="" class="btn-link btn-link_green">Saznaj više</span>
					</div>
				</div>
			</a>

		</div>

		<div class="pagination center">
			<ul class="unstyle-list">
				<li class="prev"></li>
				<span class="pagination-number">01/02</span>
				<li class="next"></li>
			</ul>
		</div>
	</section>

	<section class="background-green cta-box">
		<div class="container">
				<div class="row justify-content-end">
					<div class="col-12 col-sm-3 img">
						<img src="<?php echo get_template_directory_uri()?>/img/group_letters.svg">
					</div>
					<div class="col-12 col-sm-6">
						<h3>IMATE PITANJE ZA NAS?</h3>
						<p>Slobodno nas kontaktiraje</p>
						<a href="#" class="btn-full btn-dark btn-arrow">Pišite nam</a>
					</div>
				</div>
		</div>
	</section>

	<?php
	while ( have_posts() ) : the_post();

		//get_template_part( 'template-parts/content', 'page' );


	endwhile; // End of the loop.
	?>

<?php
get_footer();

