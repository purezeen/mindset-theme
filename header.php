<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gulp-wordpress
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>

	<!-- Google fonts Montserrat, Barlow Semi Condense -->
	<link
		href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,200,300,400,500,600|Montserrat:400,500,600,700&display=swap"
		rel="stylesheet">

	<!-- Iconify  -->
	<script src="https://code.iconify.design/1/1.0.3/iconify.min.js"></script>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site">

		<header id="masthead" class="site-header" role="banner">
			<div class="container">

				<a href="/" class="navbar-brand">
					<img src="<?php echo get_template_directory_uri(); ?>/img/mindset-white-logo.svg" alt="Minset logo">
				</a>

				<div class="burger">
					<div></div>
					<div></div>
					<div></div>
				</div>

				<div class="mobile-wrap">
					<div class="nav-close">
						+
					</div>
					<nav class="navbar navbar-expand-md navbar-ligh">
						<div class="main-menu" id="navbarCollapse">
							<?php wp_nav_menu(
							array( 
								'theme_location' => 'primary', 
								'menu_id' => 'primary-menu',
								'container' => 'ul',
								'menu_class'=> 'navbar-nav m-auto',
								'add_li_class'  => 'nav-item',
								) 
								); 
								?>
						</div>

					</nav>
					<div class="right-menu">
						<div class="lang">
							<?php do_action('wpml_add_language_selector'); ?>
						</div>

						<div class="emphasize">
							<?php wp_nav_menu(
								array( 
								'theme_location' => 'secondary', 
								'menu_id' => 'secondary-menu',
								'container' => 'ul',
								'menu_class'=> 'navbar-nav ml-auto',
								'add_li_class'  => 'nav-item',
								) 
							); 
								?>
						</div>
					</div>
				</div>


			</div>
		</header><!-- #masthead -->

		<div id="content" class="site-content">